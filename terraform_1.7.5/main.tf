data "vkcs_compute_flavor" "compute" {
  name = var.compute_flavor
}

data "vkcs_images_image" "compute" {
  name = var.image_flavor
}

resource "vkcs_compute_instance" "nginx1" {
  name = "nginx1" 
  flavor_id = data.vkcs_compute_flavor.compute.id
  key_pair= var.key_pair_name
  security_groups = ["default", "ssh", "ssh+www"] 
  availability_zone = var.availability_zone_name

  block_device {
  uuid = data.vkcs_images_image.compute.id
  source_type = "image"
  destination_type = "volume"
  volume_type = "ceph-hdd"
  volume_size = 30
  boot_index = 0
  delete_on_termination = true
  }
  
  network { 
  uuid = vkcs_networking_network.network.id 
  fixed_ip_v4 = "10.0.0.21"
  }

  user_data = "${file("${path.module}/user_data.sh")}"
}


resource "vkcs_compute_instance" "nginx2" {
  name = "nginx_2"
  flavor_id = data.vkcs_compute_flavor.compute.id
  key_pair= var.key_pair_name
  security_groups = ["default", "ssh", "ssh+www"]
  availability_zone =  var.availability_zone_name
 

  block_device {
  uuid = data.vkcs_images_image.compute.id
  source_type = "image"
  destination_type = "volume"
  volume_type = "ceph-hdd"
  volume_size = 30
  boot_index = 0
  delete_on_termination = true
  }

  
  network {
  uuid = vkcs_networking_network.network.id 
  fixed_ip_v4 = "10.0.0.22"
  }

  user_data = "${file("${path.module}/user_data.sh")}"
}

resource "vkcs_compute_instance" "reactjs" {
  name = "reactjs"
  flavor_id = data.vkcs_compute_flavor.compute.id
  key_pair= var.key_pair_name
  security_groups = ["default", "ssh", "ssh+www"]
  availability_zone =  var.availability_zone_name


  block_device {
  uuid = data.vkcs_images_image.compute.id
  source_type = "image"
  destination_type = "volume"
  volume_type = "ceph-hdd"
  volume_size = 30
  boot_index = 0
  delete_on_termination = true
  }


  network {
  uuid = vkcs_networking_network.network.id
  fixed_ip_v4 = "10.0.0.23"
  }

}


##### Balancer
resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
 # ext_subnet_id = "${vkcs_networking_network.ext-subnet.id}" 
  tags = ["tag1"]
}

resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 8080
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP" 
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_member" "member_1" {
  address = "10.0.0.21"
  protocol_port = 8080 
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}" 
  weight = 0
}

resource "vkcs_lb_member" "member_2" {
  address = "10.0.0.22"
  protocol_port = 8080 
  pool_id = "${vkcs_lb_pool.pool.id}"  
  subnet_id = "${vkcs_networking_subnet.subnetwork.id}"
}

#####
resource "vkcs_networking_floatingip" "fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  floating_ip = vkcs_networking_floatingip.fip.address
  instance_id = vkcs_compute_instance.nginx1.id
}

output "instance_fip" {
  value = vkcs_networking_floatingip.fip.address
}

resource "vkcs_networking_floatingip" "fip2" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip2" {
  floating_ip = vkcs_networking_floatingip.fip2.address 
  instance_id = vkcs_compute_instance.nginx2.id
}

output "instance_fip2" {
  value = vkcs_networking_floatingip.fip2.address
}

resource "vkcs_networking_floatingip" "fip3" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip3" {
  floating_ip = vkcs_networking_floatingip.fip3.address
  instance_id = vkcs_compute_instance.reactjs.id
}

output "instance_fip3" {
  value = vkcs_networking_floatingip.fip3.address
}

#resource "vkcs_networking_floatingip" "fip4" {
 # pool = data.vkcs_networking_network.extnet.name
#}

#resource "vkcs_compute_floatingip_associate" "fip4" {
 # floating_ip = vkcs_networking_floatingip.fip4.address
 # instance_id = vkcs_compute_instance.loadbalancer.id
#}

#output "instance_fip4" {
#  value = vkcs_networking_floatingip.fip4.address
#}


data "vkcs_lb_loadbalancer" "loadbalancer" { 
  id = vkcs_lb_loadbalancer.loadbalancer.id
  # This is unnecessary in real life. This is required here to let the example work with loadbalancer resource example.
  depends_on = [vkcs_lb_loadbalancer.loadbalancer]
}

data "vkcs_networking_port" "app-port" {
  port_id = data.vkcs_lb_loadbalancer.loadbalancer.vip_port_id
}

output "used_vips" { 

  value = data.vkcs_networking_port.app-port.all_fixed_ips
  description = "IP addresses of the app"
}
